# Web application Testing with cucumber framework using Selenium and Java

This is the test automation framework developed for web application . This automation framework is developed using selenium, cucumber, java and maven.

## Project Description

It is a behavior driven development (BDD) approach to write automation test script to test Web applications. It enables you to write and execute automated acceptance/unit tests. It is cross-platform, open source and free. Automate your test cases with minimal coding.

## Installation

Pre-requisite You need to have following softwares installed on your computer

1) Install JDK 1.8 and set path
2) Install Maven and set path
3) Eclipse
4) Eclipse Plugins: Maven and Cucumber
5) Installation In order to start using the project you need to create your own Fork on Github and then clone the project.
