package pages;

import java.util.List;

import com.cucumber.listener.Reporter;
import org.apache.xmlbeans.impl.jam.JElement;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import sun.rmi.runtime.Log;

public class Formfill {


	JavascriptExecutor js ;


	public Formfill(WebDriver driver) {
		/**
		 * Constructor
		 */

		PageFactory.initElements(driver, this);
		js = (JavascriptExecutor)driver;

	}

	@FindBy(xpath = "//h1[contains(text(),'Example')]")
	private WebElement testcafe_example;

	@FindBy(xpath = "//p[contains(text(),'This webpage is used as a sample in TestCafe tutorials.')]")
	private WebElement testcafe_desc_tutorial;

	@FindBy(xpath = "//legend[contains(text(),'Your name:')]")
	private WebElement testcafe_yourname;

	@FindBy(id = "developer-name")
	private WebElement testcafe_inputbox_yourname;

	@FindBy(id = "populate")
	private WebElement testcafe_populate_button;

	@FindBy(xpath = "//legend[contains(text(),'Which features are important to you:')]")
	private WebElement testcafe_question_featureimportant;

	@FindBy(xpath = "//*[text()='Support for testing on remote devices']")
	private WebElement testcafe_Aswer_Support;

	@FindBy(xpath = "//*[text()='Re-using existing JavaScript code for testing']")
	private WebElement testcafe_Aswer_javascript;

	@FindBy(xpath = "//*[text()='Running tests in background and/or in parallel in multiple browsers']")
	private WebElement testcafe_Aswer_Running;

	@FindBy(xpath = "//*[text()='Easy embedding into a Continuous integration system']")
	private WebElement testcafe_Aswer_EasyEmbedding;

	@FindBy(xpath = "//*[text()='Advanced traffic and markup analysis']")
	private WebElement testcafe_Aswer_AdvancedTraffic;

	@FindBy(xpath = "//*[text()='I have tried TestCafe']")
	private WebElement testcafe_haveTried;

	@FindBy(xpath = "//*[text()='Please let us know what you think:']")
	private WebElement testcafe_pleasethink_text;

	@FindBy(id = "comments")
	private WebElement testcafe_comment_inbox;

	@FindBy(id = "submit-button")
	private WebElement testcafe_sbumit_btn;

	@FindBy(xpath = "//*[text()='What is your primary Operating System:']")
	private WebElement testcafe_question_primaryOperatingSys;

	@FindBy(xpath = "//*[text()='How would you rate TestCafe on a scale from 1 to 10']")
	private WebElement testcafe_question_Howscale;

	@FindBy(id = "windows")
	private WebElement testcafe_question_primary_win;

	@FindBy(id = "macos")
	private WebElement testcafe_question_primary_mac;

	@FindBy(id = "linux")
	private WebElement testcafe_question_primary_linux;

	@FindBy(xpath = "//*[text()='Which TestCafe interface do you use:']")
	private WebElement testcafe_interface_use;

	@FindBy(xpath = "//div[@id='slider']/span")
	public WebElement slider;

	@FindBy(id = "preferred-interface")
	public WebElement dd_interface;

	@FindBy(id = "article-header")
	public WebElement validate_success;



	public void validateTextOfPage()
	{
		assertLog("Example",testcafe_example);
		assertLog("This webpage is used as a sample in TestCafe tutorials.",testcafe_desc_tutorial);
		assertLog("Your name:",testcafe_yourname);
		assertLog("Which features are important to you:",testcafe_question_featureimportant);
		assertLog("Support for testing on remote devices",testcafe_Aswer_Support);
		assertLog("Re-using existing JavaScript code for testing",testcafe_Aswer_javascript);
		assertLog("Running tests in background and/or in parallel in multiple browsers",testcafe_Aswer_Running);
		assertLog("Easy embedding into a Continuous integration system",testcafe_Aswer_EasyEmbedding);
		assertLog("Advanced traffic and markup analysis",testcafe_Aswer_AdvancedTraffic);
		assertLog("I have tried TestCafe",testcafe_haveTried);
		assertLog("How would you rate TestCafe on a scale from 1 to 10",testcafe_question_Howscale);
		assertLog("What is your primary Operating System:",testcafe_question_primaryOperatingSys);
		assertLog("Which TestCafe interface do you use:",testcafe_interface_use);
	}

	public void submitDisable()
	{
		attributevalueVerification("disabled",testcafe_sbumit_btn);
	}

	public void assertLog(String text, WebElement element)
	{
		Reporter.addStepLog("validating the text '"+text+"'  with the actual data '"+element.getText());
		Assert.assertEquals("Example",testcafe_example.getText());
	}

	public void attributevalueVerification(String atttributevalue, WebElement Expected)
	{
		String attributevalue= Expected.getAttribute(atttributevalue);
		Reporter.addStepLog("validating the attribute value for "+atttributevalue+" and actual value"+attributevalue);
		Assert.assertTrue(Boolean.parseBoolean(attributevalue));
	}

	public void selectCheckbox(String activevalue, String valueCheckbox)
	{
		if(activevalue.equalsIgnoreCase("active"))
			if ("supportTesting".equals(valueCheckbox)) {
				testcafe_Aswer_Support.click();
				Reporter.addStepLog("select the checkbox for " + testcafe_Aswer_Support.getText());
			} else if ("reuseJavascript".equals(valueCheckbox)) {
				testcafe_Aswer_javascript.click();
				Reporter.addStepLog("select the checkbox for " + testcafe_Aswer_javascript.getText());
			} else if ("reRunningBackground".equals(valueCheckbox)) {
				testcafe_Aswer_Running.click();
				Reporter.addStepLog("select the checkbox for " + testcafe_Aswer_Running.getText());
			} else if ("easyEmbedding".equals(valueCheckbox)) {
				testcafe_Aswer_EasyEmbedding.click();
				Reporter.addStepLog("select the checkbox for " + testcafe_Aswer_EasyEmbedding.getText());
			} else if ("advancedTraffic".equals(valueCheckbox)) {
				testcafe_Aswer_AdvancedTraffic.click();
				Reporter.addStepLog("select the checkbox for " + testcafe_Aswer_AdvancedTraffic.getText());
			} else {
				Reporter.addStepLog("none is selected");
			}
	}

	public void selectCheckboxIHaveTried(String value, String valueCheckbox) throws InterruptedException {
		if(value.equalsIgnoreCase("active")) {
			testcafe_haveTried.click();
		}
		float perctvalue = (Float.valueOf(valueCheckbox)/10)*100;
		String arg = "arguments[0].setAttribute('style', 'left: "+perctvalue+"%;')";
		js.executeScript(arg,slider);
		Reporter.addStepLog("Slide the screen till "+valueCheckbox);
	}

	public void selectionboxOS(String value)  {
		if(value.equalsIgnoreCase("windows"))
		{
			testcafe_question_primary_win.click();
			Reporter.addStepLog("Selected the value "+value);
		} else if(value.equalsIgnoreCase("linux"))
		{
			testcafe_question_primary_linux.click();
			Reporter.addStepLog("Selected the value "+value);
		}else
		{
			testcafe_question_primary_mac.click();
			Reporter.addStepLog("Selected the value "+value);
		}

	}
	public void dropDownvalue(String value)  {
		Select dropdown_Country = new Select(dd_interface);
		dropdown_Country.selectByVisibleText(value);
		Reporter.addStepLog("Selected dropdown value is "+value +" from TestCafe interface");
	}

	public void whatYouThink(String value)  {
		testcafe_comment_inbox.sendKeys(value);
		Reporter.addStepLog("Entered the value "+value);
	}

	public void clicksubmitbutton()  {
		testcafe_sbumit_btn.click();
		Reporter.addStepLog("clicked on the submit button");
		Assert.assertNotNull(validate_success);


	}
	public void populatevalye()  {
		testcafe_inputbox_yourname.sendKeys("Test");
		Reporter.addStepLog("Entered the populate value Test text box");
	}

	public void clickPopulatebutton()
	{
		testcafe_populate_button.click();
		Reporter.addStepLog("clicked on populate button");

	}
}
