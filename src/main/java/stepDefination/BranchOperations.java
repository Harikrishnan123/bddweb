package stepDefination;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.AfterClass;
import org.openqa.selenium.WebDriver;

import com.cucumber.listener.Reporter;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import pages.Formfill;
import utility.DriverFactory;
import utility.Utility;


public class BranchOperations {
	
	public WebDriver driver;
	Formfill formfill;
	public List<HashMap<String, String>> datamap;
	String id;
	
	static Logger logger = Logger.getLogger(BranchOperations.class.getName());



	
	@Before
	public void setUp() {
		Reporter.assignAuthor("Test");
		logger.info("Instantiating chrome driver");
		driver = DriverFactory.get_driver_instance();
		formfill = new Formfill(driver);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}
	
	
	@After
	public void tearDown() {
	    Reporter.loadXMLConfig(new File(System.getProperty("user.dir")+Utility.getProperty("reportConfigPath")));
	    Reporter.setSystemInfo("User Name", Utility.getProperty("uname"));
	    Reporter.setSystemInfo("Time Zone", System.getProperty("user.timezone"));
	    Reporter.setSystemInfo("Machine", "Windows 10" + "64 Bit");
	    Reporter.setSystemInfo("Selenium", "3.141.59");
	    Reporter.setSystemInfo("Maven", "3.5.2");
	    Reporter.setSystemInfo("Java Version", "1.8.0_151");
	    driver.close();
	    driver.quit();
	}


	@Given("^user navigates to the express application$")
	public void user_is_on_the_application_home_page() throws Throwable {
		driver.get(Utility.getProperty("url"));
	}

	@When("^I validate the text in the page$")
	public void ivalidate_the_text_page() throws Throwable {
		Reporter.addStepLog("formfill to the application");
		formfill.validateTextOfPage();

	}

	@Then("^I validate the submit button is in disable$")
	public void Ivalidate_submitbutton_disable()
	{
		formfill.submitDisable();
	}

	@Then("^I validate the checkbox status \"([^\"]*)\" for \"([^\"]*)\"$")
	public void validate_checkbox_active_deactive(String ActiveDeactive, String checkboxvalue)
	{
		formfill.selectCheckbox(ActiveDeactive, checkboxvalue);
	}

	@Then("^I activate the checkbox \"([^\"]*)\" and scroll to the value \"([^\"]*)\"$")
	public void validate_checkbox_scroll_to_element(String ActiveDeactive, String checkboxvalue) throws InterruptedException {
		formfill.selectCheckboxIHaveTried(ActiveDeactive, checkboxvalue);
	}

	@Then("^I enter the value in the inputbox \"([^\"]*)\"$")
	public void enter_value_InputBox(String value) throws Throwable {
		formfill.whatYouThink(value);
	}
	@Then("^I select the value \"([^\"]*)\" for the operating system$")
	public void selectionBox(String value) throws Throwable {
		formfill.selectionboxOS(value);
	}

	@Then("^I select the value from dropdown tescafe interface \"([^\"]*)\"$")
	public void dropdownValue(String value) throws Throwable {
		formfill.dropDownvalue(value);
	}

	@Then("^I click on submit button$")
	public void clcikSubmit() throws Throwable {
		formfill.clicksubmitbutton();
		user_is_on_the_application_home_page();
	}

	@Given("^user enter the value in the populate$")
	public void enterPopulatevalue() throws Throwable {
		formfill.populatevalye();
	}

	@Given("^I validate the popup$")
	public void validateAlert() throws Throwable {
		formfill.clickPopulatebutton();
	}


}
