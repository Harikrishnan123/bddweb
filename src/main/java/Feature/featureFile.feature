Feature: validate the application by filling the form
  Scenario: Verify the text displays properly in the page.
    Given user navigates to the express application
    When I validate the text in the page
    Then I validate the submit button is in disable

#  Scenario Outline: Verify by validating all the combination with valid data.
#    Given user navigates to the express application
#    When I validate the text in the page
#    Then I validate the submit button is in disable
#    Given user enter the value in the populate
#    Then I validate the checkbox status "<FeatureStatus>" for "<FeatureImportant>"
#    Then I select the value "<OS>" for the operating system
#    Then I select the value from dropdown tescafe interface "<Use>"
#    Then I activate the checkbox "<TestCafeCheckbox>" and scroll to the value "<TestCafeScroll>"
#    Then I enter the value in the inputbox "<Inputboxvalue>"
#    Then I click on submit button
#    Examples:
#      |FeatureImportant   |FeatureStatus|TestCafeCheckbox| TestCafeScroll |Inputboxvalue | OS    |Use  |
#      |supportTesting     |Active       |Active         |1               |Test          |windows|Both |
#      |reuseJavascript    |Active       |Active         |2               |Test          |linux  |Command Line  |
#      |reRunningBackground|Active       |Active         |3               |Test          |mac    |JavaScript API |
#      |easyEmbedding      |Active       |Active         |4               |Test          |windows|Both |
#      |advancedTraffic    |Active       |Active         |5               |Test          |linux  |Command Line  |
#      |supportTesting     |Active       |Active         |6               |Test          |mac    |JavaScript API |
#      |reuseJavascript    |Active       |Active         |7               |Test          |windows|Both |
#      |reRunningBackground|Active       |Active         |8               |Test          |linux  |Command Line  |
#      |easyEmbedding      |Active       |Active         |9               |Test          |mac    |JavaScript API |
#
#
